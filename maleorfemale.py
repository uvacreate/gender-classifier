import os
import csv
from lxml import etree

import pickle
import numpy as np
import pandas as pd

import sklearn
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder

from sklearn.feature_extraction import DictVectorizer
from sklearn.feature_extraction.text import CountVectorizer

import tensorflow as tf
import keras.backend as K
from keras.models import Sequential
from keras.layers import Dense, Dropout

DATADIR = "data"
DOOPREGISTERS = "SAA_Index_op_doopregister_20181004"
ONDERTROUWREGISTERS = "SAA_Index_op_ondertrouwregister_20181005"
ECARTICO = "ECARTICO"

def parse_name(data):
    
    patronym = ""
    
    first_names = data.xpath('voornaam')
    if first_names:
        first_name = first_names[0].text        
    else:
        first_name = ""

    infixes = data.xpath('tussenvoegsel')
    if infixes:
        infix_name = infixes[0].text
    else:
        infix_name = ""

    family_names = data.xpath('achternaam')
    if family_names:
        family_name = family_names[0].text
    else:
        family_name = ""

    name = (first_name, infix_name, family_name, patronym)
    
    return name

def parse_doop(xmlfile):
    
    tree = etree.parse(xmlfile)
    
    males = tree.xpath('//vader')
    females = tree.xpath('//moeder')
    
    male_names = [parse_name(m) for m in males]
    female_names = [parse_name(f) for f in females]

    return male_names, female_names
    
def parse_trouw(xmlfile):
    
    tree = etree.parse(xmlfile)
    
    males = tree.xpath('//naamBruidegom') + tree.xpath('//naamEerdereMan')
    females = tree.xpath('//naamBruid') + tree.xpath('//naamEerdereVrouw')
    
    male_names = [parse_name(m) for m in males]
    female_names = [parse_name(f) for f in females]
    
    return male_names, female_names

def parse_ecartico(csvfilename):
    
    male_names = []
    female_names = [] 
    
    with open(csvfilename) as csvfile:
        for r in csv.reader(csvfile):
            if r[0] == 'male':
                male_names.append(tuple(r[1:]))
            elif r[0] == 'female':
                female_names.append(tuple(r[1:]))
    
    return male_names, female_names

def collect_names(DATADIR):
    """
    Build a list of names from the files in de datadir.
    """
    all_male_names = []
    all_female_names = []

    for file in os.listdir(DATADIR):

        filename = os.path.join(DATADIR,file)

        # Doopregisters
        if file.startswith(DOOPREGISTERS):
            male_names, female_names = parse_doop(filename)

        # Ondertrouwregisters
        elif file.startswith(ONDERTROUWREGISTERS):
            male_names, female_names = parse_trouw(filename)

        # Ecartico
        elif file.startswith(ECARTICO):
            male_names, female_names = parse_ecartico(filename)

        # Bring everything together
        all_male_names += male_names
        all_female_names += female_names
        
    return all_male_names, all_female_names
    
def print_stats(all_male_names, all_female_names):
    print("Some statistics:\n---------------")
    print("Number of male names:\t",len(all_male_names))
    print("Number of female names:\t",len(all_female_names))
    print("Total number of names:\t", len(all_female_names)+len(all_male_names))

def build_training_set(DATADIR, filename='training.tsv'):
    """
    Output a tsv file with all name information from the datafiles,
    including a gender. 
    """
    all_male_names, all_female_names = collect_names(DATADIR)
    print_stats(all_male_names, all_female_names)
    
    with open(filename, 'w') as outfile:

        outfile.write("gender\tfirst_name\tinfix_name\tlast_name\tpatronym\n")

        for (first_name, infix_name, family_name, patronym) in all_male_names:
            if first_name:
                outfile.write(f"M\t{first_name}\t{infix_name}\t{family_name}\t{patronym}\n")

        for (first_name, infix_name, family_name, patronym) in all_female_names:
            if first_name:
                outfile.write(f"F\t{first_name}\t{infix_name}\t{family_name}\t{patronym}\n")

def prepare_training(trainingfile):

    data = pd.read_csv(trainingfile, header=0, sep='\t', low_memory=False)
    data["gender"] = data["gender"].astype("category")

    print(data.head(5))
    print(data.describe())

    return data

def build_vectorizers(data):

    y = data.gender.values
    encoder = LabelEncoder()
    encoder.fit(y)
    y = encoder.transform(y)

    # first_names = [name[0] for name in [n.split(" ") for n in data['first_name']]]
    # first_names_eof = [name + '@' for name in first_names]

    # Append a `@` to a name to mark the end of a word so that the ngrams created by the vectorizer also takes the position of the suffix (=at the end) into account as feature. 
    first_names_split_eof = [" ".join(names) for names in [[i + '@' for i in sublist] for sublist in [name.split(' ') for name in data['first_name']]]]

    ngram_counter = CountVectorizer(ngram_range=(2, 4), analyzer='char')
    X = ngram_counter.fit_transform(first_names_split_eof)

    return X, y, ngram_counter, encoder

def f1(y_true, y_pred):
    """
    Taken from https://www.kaggle.com/guglielmocamporese/macro-f1-score-keras
    """
    y_pred = K.round(y_pred)
    tp = K.sum(K.cast(y_true*y_pred, 'float'), axis=0)
    # tn = K.sum(K.cast((1-y_true)*(1-y_pred), 'float'), axis=0)
    fp = K.sum(K.cast((1-y_true)*y_pred, 'float'), axis=0)
    fn = K.sum(K.cast(y_true*(1-y_pred), 'float'), axis=0)

    p = tp / (tp + fp + K.epsilon())
    r = tp / (tp + fn + K.epsilon())

    f1 = 2*p*r / (p+r+K.epsilon())
    f1 = tf.where(tf.is_nan(f1), tf.zeros_like(f1), f1)
    return K.mean(f1)

def train(X, y, test_size=0.2):

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size, random_state=42)

    model = Sequential()
    model.add(Dense(512, input_dim=X.shape[1], activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(128, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(32, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(1, activation='sigmoid'))

    model.compile(loss='binary_crossentropy',
                  optimizer='rmsprop',
                  metrics=['accuracy'])

    model.fit(X_train, y_train, epochs=1, batch_size=128)

    return model

if __name__ == "__main__":

    TRAININGFILE = 'training/training.tsv'

    print("Building training set")
    # build_training_set(DATADIR, filename=TRAININGFILE)

    print("Reading training set")
    data = prepare_training(TRAININGFILE)

    print("Preparing and vectorizing training set")
    X, y, ngram_counter, encoder = build_vectorizers(data)

    print("Training the model!")
    model = train(X, y, test_size=0.0)

    with open('model/encoder_y.pickle', 'wb') as picklefile:
        pickle.dump(encoder, picklefile)
        
    with open('model/vectorizer_x.pickle', 'wb') as picklefile:
        pickle.dump(ngram_counter, picklefile)
        
    model.save('model/gender-classifier.keras')
    print("Model saved to 'model/gender-classifier.keras'")