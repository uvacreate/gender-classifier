# Gender Classifier

Classify person gender based on first name. 

## Corpus

All files from:

* SAA_Index_op_doopregister_20181004
* SAA_Index_op_ondertrouwregister_20181005
* Ecartico (dump 2019/01/25)



Set | Count
---- | ----
Number of male names | 1.892.993
Number of female names | 1.826.694
Total | 3.719.687


## Score
Training on 80% of the data, validating on 20%:

Features | Feature count | Loss | Accuracy | F1
---- | ---- | ---- | ---- | ----
n-grams (2-4) | 55850 | 0.0481 | 0.9904 | 0.9905
n-grams (2-4) first word only | 39065 | 0.0514 | 0.9902 | 0.9903
n-grams (2-4) with end of line-@ | 60417 | **0.0436** | 0.9908 | 0.9909
n-grams (2-4) with end of word-@ | 52365 | 0.0470 | **0.9909** | **0.9910**
n-grams (2-4) first word only with end of word-@ | 43247 | 0.0463 | 0.9904 | 0.9905


